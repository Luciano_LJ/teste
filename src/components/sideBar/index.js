import UserImg from '../../assets/images/user.svg';

function SideBar() {
	return (
		<>
            <div className="side-bar">
                <div className="topo">
                    <img src={UserImg} alt="imagem de usuário" width="30" />
                    <p>Olá Fulano!</p>
                </div>

                <div class="navigator">
                    <ul>
                        <li>
                            <a href="">Home</a>
                        </li>

                        <li>
                            <a href="">Perfil</a>
                        </li>

                        <li>
                            <a href="">Certificados</a>
                        </li>

                        <li>
                            <a href="">Cadastrar</a>
                        </li>

                        <li>
                            <a href="">Histórico</a>
                        </li>
                    </ul>
                </div>
            </div>
		</>
	);
}

export default SideBar;
