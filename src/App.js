import { BrowserRouter as Router } from "react-router-dom";
import Routes from './pages/rotas';

const App = () => {
    return (
        <>
            <Router>
                <Routes />
            </Router>
        </>
    );
}

export default App;
