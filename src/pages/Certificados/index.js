import { useEffect, useState } from 'react';
import SideBar from '../../components/sideBar/index';
import certificadosService from '../../services/certificadosService';

const Certificados = (props) => {

    const [certificados, setCertificados] = useState([]);

    useEffect(() => {
        let certificados = async function certificados(){
            return certificadosService.buscarCertificados();
        }
        setCertificados(certificados)
        console.log(certificados)
    }, []);

    return (
        <>
            <section className="page-home row">
                <SideBar />
                <div className="container">
                    <table>
                        <thead>
                            <th>
                                <td>Nome do evento</td>
                                <td>Data</td>
                                <td>Horas</td>
                                <td>Excluir</td>
                            </th>
                        </thead>
                        <tbody>
                            {/* {certificados.map(certificado => (
                                <tr key={certificado.id}>
                                    <td>{certificado.nome}</td>
                                    <td>{certificado.dataInicio}</td>
                                    <td>{certificado.horas}</td>
                                    <td>
                   
                                    </td>
                                </tr>
                            ))} */}
                        </tbody>
                    </table>
                </div>
            </section>
		</>
    );
}

export default Certificados;