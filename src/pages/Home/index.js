import SideBar from '../../components/sideBar/index';

const Home = () => {
    return (
        <>
            <section className="page-home row">
                <SideBar />
                <div className="container">
                    
                </div>
            </section>
		</>
    );
}

export default Home;