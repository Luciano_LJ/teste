import CaixaCertificados from '../../assets/images/BoasVindas/caixa-certificados.svg';

function App() {
	return (
		<>
			<section className="boas-vindas">
				<div className="container">
					<div className="content">
						<div className="textos">
							<div className="borda-branca"></div>

							<h2>
								Sua ferramenta <br/>
								Inteligente para <br/>
								armazenamento <br/>
								de certificados  <br/>
								acadêmicos! <br/>
							</h2>
						</div>

						<div className="caixa-certificados">
							<img src={CaixaCertificados} alt="Caixa com Certificados" />
						</div>
					</div>

					<div className="buttons">
						<a href="/cadastro" className="btn btn-acao">Cadastre-se</a>
						<a href="/login" className="btn btn-redirect">Entrar</a>
					</div>
				</div>

				<div className="faixa-branca"></div>
			</section>
		</>
	);
}

export default App;
