import SideBar from '../../components/sideBar/index';
import UserImg from '../../assets/images/user-black.svg';
import InstituicaoImg from '../../assets/images/insituicao.svg';

const Perfil = (props) => {

    return (
        <>
           <section className="page-perfil row">
                <SideBar />
                <div className="container-page-interna">
                    <div className="div-img">
                        <img className="img-user" src={UserImg} alt="Usuario" />
                        {/* <img className="img-user" src={InstituicaoImg} alt="Instituição" /> */}
                    </div>
                
                    <div className="informacoes">
                        <div className="div-informacao">
                            <p className="label">Nome</p>
                            <p className="informacao">
                                Fulano de tal
                            </p>
                        </div>

                        <div className="div-informacao">
                            <p className="label">CPF</p>
                            <p className="informacao">
                                888.888.888-25
                            </p>
                        </div>

                        <div className="div-informacao">
                            <p className="label">E-mail</p>
                            <p className="informacao">
                                Fulanodetal@gmail.com
                            </p>
                        </div>

                        <div className="div-informacao">
                            <p className="label">Telefone</p>
                            <p className="informacao">
                                48 99999-9999
                            </p>
                        </div>

                        <div className="div-informacao">
                            <p className="label">Instituição</p>
                            <p className="informacao">
                                UFRJ
                            </p>
                        </div>  

                        <div className="div-informacao">
                            <p className="label">Área de Conhecimento</p>
                            <p className="informacao">
                                Matemática (licenciatura)
                            </p>
                        </div>
                    </div>
                </div>
            </section>
		</>
    );
}

export default Perfil;