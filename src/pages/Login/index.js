const Login = () => {
    return (
        <>
            <section className="login-page">
                
                <div className="title-page">
                    <h2>LOGIN</h2>
                </div>

                <div className="container">
                    <form name="form-login" id="form-login">
                        <div className="row width-50 mg-bt-1">
                            <div className="col width-100">
                                <label>CPF</label>
                                <input type="text" placeholder="000-000-00" />
                            </div>
                        </div>

                        <div className="row width-50">
                            <div className="col width-100">
                                <label>SENHA</label>
                                <input type="password" placeholder="******"/>
                            </div>
                        </div>
                    </form>

                    <div className="buttons">
                        <button type="submit" className="btn btn-acao" form="form-login">Entrar</button>
                    </div>
                </div>

                <div className="faixa-branca"></div>
            </section>
		</>
    );
}

export default Login;