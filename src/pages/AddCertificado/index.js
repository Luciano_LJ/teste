import {useForm} from 'react-hook-form';
import certificadoService from '../../services/certificadosService';
import SideBar from '../../components/sideBar/index';

const AddCertificado = () => {

    const {register, handleSubmit} = useForm();
    const onSubmit = certificado => {certificadoService.adicionarCertificado(certificado); console.log('error')}; 

    return (
        <>
            <section className="cadastro-certificados-page">
                <SideBar />
                <div className="container-page-interna">
                    <div className="title-page">
                        <h2>Insira os dados solicitados abaixo e cadastre seu certificado acadêmico!</h2>
                    </div>

                    <form onSubmit={handleSubmit(onSubmit)} name="certificados" id="form-certificados">
                        <div className="row width-100 mg-bt-1">
                            <div className="col mg-rgt-2">
                                <label>Nome do evento</label>
                                <input type="text" {...register("eventName")} />
                            </div>

                            <div className="col mg-rgt-2 ">
                                <label>Data de início</label>
                                <input type="text" placeholder="00/00/0000" {...register("beginData")} />
                            </div>

                            <div className="col mg-rgt-2">
                                <label>Data do fim</label>
                                <input type="text" placeholder="00/00/0000" {...register("endDate")} />
                            </div>

                            <div className="col">
                                <label>Horas</label>
                                <input type="text" placeholder="exemplo: 100" {...register("hours")} />
                            </div>
                        </div>

                        <div className="row width-100 mg-bt-2">
                            <div className="col mg-rgt-2">
                                <label>País</label>
                                <input type="text" {...register("country")} />
                            </div>

                            <div className="col mg-rgt-2">
                                <label>Cidade</label>
                                <input type="text" {...register("city")} />
                            </div>

                            <div className="col">
                                <label>Comissão organizadora</label>
                                <input type="text" placeholder="exemplo: UFSC" {...register("idealizer")} />
                            </div>
                        </div>

                        <div className="buttons">
                            <button type="submit" className="btn btn-acao mg-0">Cadastrar</button>
                        </div>
                    </form>
                </div>

                <div className="faixa-verde"></div>
            </section>
		</>
    );
}

export default AddCertificado;