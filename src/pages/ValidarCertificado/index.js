import SideBar from '../../components/sideBar/index';

const ValidacaoCertificados = (props) => {

    return (
        <>
            <section className="page-validacao-certificados row">
                <SideBar />
                <div className="container-page-interna">
                    <div className="title-page">
                        <h2>Realize a validação dos Certificados solicitados abaixo</h2>
                    </div>
                
                    <table>
                        <thead>
                            <tr>
                                <th>Nome do evento</th>
                                <th>Data</th>
                                <th>Horas</th>
                                <th>Excluir</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>NULL</td>
                                <td>NULL</td>
                                <td>NULL</td>
                                <td>NULL</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </section>
        </>
    );
}

export default ValidacaoCertificados;