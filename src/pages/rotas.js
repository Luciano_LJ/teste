import { Routes, Route } from "react-router-dom";
import BoasVindas from './BoasVindas/index';
import Login from './Login/index';
import Cadastro from './Cadastro/index';
import Home from './Home/index';
import CadastroCertificados from './AddCertificado/index';
import Certificados from './Certificados/index';
import Perfil from './Perfil/index';
import ValidacaoCertificados from "./ValidarCertificado";



export default function MainRoutes() {
    return (
        <Routes>
            <Route path="/certistack" element={< BoasVindas />} />
            <Route path="/certistack/login" element={< Login />} />
            <Route path="/certistack/cadastro" element={< Cadastro />} />
            <Route path="/certistack/home" element={< Home />} />
            <Route path="/certistack/cadastro-certificados" element={< CadastroCertificados />} />
            <Route path="/certistack/certificados" element={< Certificados />} />
            <Route path="/certistack/validacao-certificados" element={< ValidacaoCertificados />} />
            <Route path="/certistack/perfil" element={< Perfil />} />
        </Routes>
    );
}