import {useForm} from 'react-hook-form';
import usuarioService from '../../services/usuarioService';

const Cadastro = () => {

    const {register, handleSubmit} = useForm();
    const onSubmit = usuario => usuarioService.adicionarUsuario(usuario); 

    return (
        <>
            <section className="cadastro-page">
                <div className="title-page">
                    <h2>CADASTRO</h2>
                </div>

                <div className="container">
                    <div className="">
                        <div className="row mg-bt-2">
                            <div className="row mg-rgt-2">
                                <input type="radio" name="cpf-cnpj" id="radio-cpf" />
                                <label for="radio-cpf">DISCENTE</label>
                            </div>

                            <div className="row">
                                <input type="radio" name="cpf-cnpj" id="radio-cnpj" />
                                <label for="radio-cnpj">INSTITUIÇÃO</label>
                            </div>
                        </div>
                    </div>

                    <form onSubmit={handleSubmit(onSubmit)} name="form-login" id="form-cadastro">
                        <div className="row justify-space-between width-100 mg-bt-1">
                            <div className="col-3 coletion-cpf">
                                <label>NOME</label>
                                <input type="text" placeholder="Digite seu nome" {...register("name")} />
                            </div>

                            <div className="col-3 coletion-cnpj">
                                <label>NOME DA INSTITUIÇÃO</label>
                                <input type="text" placeholder="Digite o nome da instituição" {...register("name")} />
                            </div>

                            <div className="col-3 coletion-cpf">
                                <label>CPF</label>
                                <input type="text" placeholder="000-000-00" {...register("cpf")} />
                            </div>

                            <div className="col-3 coletion-cnpj">
                                <label>CNPJ</label>
                                <input type="text" placeholder="000-000-00" {...register("cnpj")} />
                            </div>

                            <div className="col-3">
                                <label>E-MAIL</label>
                                <input type="text" placeholder="Digite seu e-mail" {...register("email")} />
                            </div>
                        </div>

                        <div className="row justify-space-between width-100 mg-bt-1">
                            <div className="col-3">
                                <label>TELEFONE</label>
                                <input type="text" placeholder="(00) 00000-0000" {...register("phone")} />
                            </div>

                            <div className="col-3">
                                <label>SENHA</label>
                                <input type="password" placeholder="******" {...register("senha")} />
                            </div>

                            <div className="col-3">
                                <label>CONFIRME SUA SENHA</label>
                                <input type="password" placeholder="******" />
                            </div>
                        </div>

                        <div className="row justify-space-between width-100">
                            <div className="col-3 coletion-cpf">
                                <label>INSTITUIÇÃO</label>
                                <select type="text" placeholder="Digite o nome da instituição" {...register("instituicao")}> 
                                    <option value="" key="">-</option>
                                </select>
                            </div>

                            <div className="col-3 coletion-cnpj">
                                <label>DEPARTAMENTO</label>
                                <input type="text" placeholder="Digite o nome do departamento" {...register("departamento")} />
                            </div>

                            <div className="col-3">
                                <label>ÁREA DE CONHECIMENTO</label>
                                <select {...register("areaEducacao")}>
                                    <option value="" key="">-</option>
                                </select>
                            </div>

                            <div className="col-3 coletion-cpf">
                                <label>LATTES</label>
                                <select type="text" placeholder="Digite seu lattes" {...register("lattes")}> 
                                    <option value="" key="">-</option>
                                </select>
                            </div>
                        </div>
                    </form>

                    <div className="buttons">
                        <button type="submit" className="btn btn-acao" form="form-cadastro">Cadastrar</button>
                    </div>
                </div>

                <div className="faixa-branca"></div>
            </section>
		</>
    );
}

export default Cadastro;