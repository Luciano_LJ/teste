import axios from 'axios';
import Environments from '../environments/environments';

const BASE_URL = `${Environments.api_url}/certificados`;

class CertificadosService {
    buscarCertificados = () => {
        return axios.get(BASE_URL)
            .then(res => console.log(res.data))
            .catch(error => console.log(error))
    }

    buscarCertificado = (id) => {
        return axios.get(`${BASE_URL}/${id}`)
            .then(res => console.log(res.data))
            .catch(error => console.log(error))
    }

    adicionarCertificado = (certificado) => {
        console.log(Environments.api_url);
        return axios.post(BASE_URL, certificado)
            .then(res => console.log(res.data))
            .catch(error => console.log(error))
    }

    editarCertificados = (certificado, id) => {
        return axios.put(`${BASE_URL}/${id}`, certificado)
            .then(res => console.log(res.data))
            .catch(error => console.log(error))
    }

    deletarCertificados = (id) => {
        return axios.delete(`${BASE_URL}/${id}`)
            .then(res => console.log(res.data))
            .catch(error => console.log(error))
    }
}

export default new CertificadosService();