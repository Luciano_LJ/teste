import axios from 'axios';

const BASE_URL = `${process.env.CERTISTACK_API}/certistack`;

class UsuarioService {
    buscarUsuarios = () => {
        return axios.get(BASE_URL)
            .then(res => console.log(res.data))
            .catch(error => console.log(error))
    }
    buscarUsuario = (id) => {
        return axios.get(`${BASE_URL}/${id}`)
            .then(res => console.log(res.data))
            .catch(error => console.log(error))
    }

    adicionarUsuario = (usuario) => {
        return axios.post(BASE_URL, usuario)
            .then(res => console.log(res.data))
            .catch(error => console.log(error))
    }

    editarUsuario = (usuario, id) => {
        return axios.put(`${BASE_URL}/${id}`, usuario)
            .then(res => console.log(res.data))
            .catch(error => console.log(error))
    }

    deletarUsuario = (id) => {
        return axios.delete(`${BASE_URL}/${id}`)
            .then(res => console.log(res.data))
            .catch(error => console.log(error))
    }
}

export default new UsuarioService();